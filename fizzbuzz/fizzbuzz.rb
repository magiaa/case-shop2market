FIZZBUZZ_MAPPING = { 15 => 'FizzBuzz', 3 => 'Fizz', 5 => 'Buzz' }

def divisible_by(number, divisor)
  number % divisor == 0
end

def fizzbuzz(number)
  FIZZBUZZ_MAPPING.each do |divisor, word|
    return word if divisible_by(number, divisor)
  end
  number
end

(1..100).each do |f|  puts fizzbuzz(f) end

