require_relative 'fizzbuzz'

describe 'fizzbuzz' do

  context 'when the number is divisible by 15' do
    it { expect(fizzbuzz(15)).to eq 'FizzBuzz' }
  end

  context 'when the number is divisible by 3 but not by 15' do
    it { expect(fizzbuzz(3)).to eq 'Fizz' }
  end

  context 'when the number is divisible by 5 but not by 15' do
    it { expect(fizzbuzz(5)).to eq 'Buzz' }
  end

  context 'when the number is not divisible by 3 nor 5' do
    it 'returns the number' do
      expect(fizzbuzz(1)).to eq 1
    end
  end

end